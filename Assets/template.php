<?php
/**
 * Slider column
 *
 * @package ChefOehlala
 * @since 2016
 */

	use ChefSections\Wrappers\Template;
	use Cuisine\View\Image;
	use Cuisine\Wrappers\Field;
	use chefSlider\Classes\Column;
	use Cuisine\Utilities\Sort;
	
	$media = $column->getField( 'media' );
	$pagination = $column->getField('paginationThumb');
	$media = Sort::byField( $media, 'position', 'ASC' );

if( !empty( $media ) ){
	
	echo '<div class="column slider-column">';

		$column->theTitle();
	
		echo '<div class="'.$column->sliderClass().'">';
	
			foreach( $media as $img ){
					
				$full = Image::getMediaUrl( $img['img-id'], 'full' );
				$med = Image::getMediaUrl( $img['img-id'], 'medium' );

				if( !$med || $med == '' )
					$med = $column->getField( 'full' );

				$altTag = '';

				//Get the ID and other data
				$imageID = $img['img-id'];
				$postSrc = get_post( $imageID );
				$postMeta = get_post_meta( $imageID );

				//get the alt and title fields
				$title = $postSrc->post_title;
				$description = $postSrc->post_content;

				if( isset( $postMeta['_wp_attachment_image_alt'] ) ){ 
					$altTag = $postMeta['_wp_attachment_image_alt']['0'];
				}

				echo '<div class="gallery-cell">';
					echo '<img src="'.$med.'" title="'.$title.'" alt="'.$altTag.'" data-flickity-lazyload="'.$full.'"/>';
				echo '</div>';
			}
	
		echo '</div>';
		
		if ( $pagination == "thumbs" ) {

			echo '<div class="gallery-nav fullwidth gallery-slider">';

				foreach( $media as $img ){

					$altTag = '';

					//Get the ID and other data
					$imageID = $img['img-id'];
					$postSrc = get_post( $imageID );
					$postMeta = get_post_meta( $imageID );

					//get the alt and title fields
					$title = $postSrc->post_title;
					$description = $postSrc->post_content;

					if( isset( $postMeta['_wp_attachment_image_alt'] ) ){ 
						$altTag = $postMeta['_wp_attachment_image_alt']['0'];
					}
					
					$thumb = Image::getMediaUrl( $img['img-id'], 'thumbnail' );
	
					echo '<img class="gallery-cell" src="'.$thumb.'" title="'.$title.'" alt="'.$altTag.'" data-flickity-lazyload="'.$thumb.	'"/>';
	
				}
			echo '</div>';

		}

	echo '</div>';
	
}