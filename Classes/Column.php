<?php
namespace ChefSlider;

use ChefSections\Columns\DefaultColumn;
use Cuisine\Wrappers\Field;
use Cuisine\Wrappers\Script;
use Cuisine\Utilities\Url;


class Column extends DefaultColumn{

	/**
	 * The type of column
	 * 
	 * @var String
	 */
	public $type = 'slider';


	/*=============================================================*/
	/**             Template                                       */
	/*=============================================================*/


	/**
	 * Start the slider wrapper
	 * 
	 * @return string ( html, echoed )
	 */
	public function beforeTemplate(){
		

	}



	/**
	 * Add javascripts to the footer, before the template
	 * and close the div wrapper
	 * 
	 * @return string ( html, echoed )
	 */
	public function afterTemplate(){

		$url = Url::plugin( 'chef-slider', true ).'Assets/js/libs/';
		Script::register( 'flickity', $url.'flickity', false );

		$url = Url::plugin( 'chef-slider', true ).'Assets/js/';
		Script::register( 'trigger-script', $url.'trigger', true );
		
		$this->setSliderVars();
			
	}

	/**
	 * Add the javascript vars for this column
	 * 
	 * @return void
	 */
	private function setSliderVars(){

		//set variables object:
		$pagination = $this->getField( 'paginationThumb' );
		$alignment = $this->getField( 'cellAlign' );

		$speed = 0;
		$wrap = false;
		$hidePagination = true;
		$navButtons = false;

		if( $this->getField( 'sliderSpeed') &&  intval($this->getField( 'sliderSpeed'))>=3000) {
			$speed =  intval( $this->getField( 'sliderSpeed' ) );
		}
		
		if( $this->getField( 'wrapAround' ) && $this->getField('wrapAround') != "false" ) {
			$wrap =  true;
		}

		if ( $this->getField('navButtons') != "false" ){
			$navButtons = true;
		}
		
		if( $pagination == "thumbs" ) {
			$hidePagination =  false;
		
		}else if( $pagination == "none" ) {
			$hidePagination =  false;
		
		}else if( $pagination == "dots" ) {
			$hidePagination =  true;
		
		}

		$vars = array(
			'speed' 			=> $speed,
			'wrap' 				=> $wrap,
			'hidePagination' 	=> $hidePagination,
			'alignment'			=> $alignment,
			'navButtons'		=> $navButtons
		);

		Script::variable( 'Slider', $vars );
	}


	/*=============================================================*/
	/**             Backend                                        */
	/*=============================================================*/


	/**
	 * Create the preview for this column
	 * 
	 * @return string (html,echoed)
	 */
	public function buildPreview(){

		echo $this->getTitle( 'title' );

		$media = $this->getField( 'media' );
		$i = 0;

		if( !empty( $media ) ){

			echo '<div class="slide-thumbs">';

			foreach( $media as $item ){

				if( $i < 5 ){

					echo '<span class="slide-thumb">';

						echo '<img src="'.$item['preview'].'">';

					echo '</span>';
					
					$i++;
				}
			}

			echo '</div>';

		}
	}


	/**
	 * Build the contents of the lightbox for this column
	 * 
	 * @return string ( html, echoed )
	 */
	public function buildLightbox(){

		$fields = $this->getFields();
		$subfields = $this->getSubFields();

		echo '<div class="main-content">';
		
			foreach( $fields as $field ){

				$field->render();

				if( method_exists( $field, 'renderTemplate' ) ){
					echo $field->renderTemplate();
				}

			}

		echo '</div>';
		echo '<div class="side-content">';
			echo '<br/><br/>';
			foreach( $subfields as $field ){

				$field->render();

			}

			$this->saveButton();

		echo '</div>';
	}


	/**
	 * Get the fields for this column
	 * 
	 * @return [type] [description]
	 */
	public function getFields(){
		$numberOfSlides = array(
					'images-1' 		=> __( '1 afbeelding', 'chefslider' ),
					'images-2'		=> __( '2 afbeeldingen', 'chefslider' ),
					'images-3'		=> __( '3 afbeeldingen', 'chefslider' ),
					'images-4'		=> __( '4 afbeeldingen', 'chefslider' ),
					'images-5'		=> __( '5 afbeeldingen', 'chefslider' )
		);

		$numberOfSlides = apply_filters( 'chef_slider_slides_number', $numberOfSlides );

		$fields = array(
			'title' => Field::title( 
				'title', 
				'',
				array(
					'label' 				=> false,
					'placeholder' 			=> 'Titel',
					'defaultValue'			=> $this->getField( 'title', ['text' => '', 'type' => 'h2'] ),
				)
			),
			'media' => Field::media( 
				'media', //this needs a unique id 
				'Media', 
				array(
					'label'				=> 'top',
					'defaultValue' 		=> $this->getField( 'media' )
				)
			),
			'numberSlides'	=> Field::select(
				'numberSlides',
				__( 'Hoeveel afbeeldingen per rij', 'chefslider' ),
				$numberOfSlides,
				array(
					'defaultValue' => $this->getField( 'numberSlides', '2-images' )
				)
			)
		);

		return $fields;

	}

	/**
	 * Get all the subfields
	 * 
	 * @return array
	 */
	private function getSubFields(){

		$view = array(
					'none' 		=> __( 'Geen', 'chefslider' ),
					'dots'		=> __( 'Bolletjes', 'chefslider' ),
					'thumbs'	=> __( 'Thumbnail', 'chefslider' )
		);

		$alignment = array(
					'left' 		=> __( 'Links', 'chefslider' ),
					'center'	=> __( 'Midden', 'chefslider' ),
					'right'		=> __( 'Rechts', 'chefslider' )
		);

		$fields = array(
			'sliderSpeed' => Field::number( 
				'sliderSpeed', 
				__( 'Snelheid van de slider', 'chefslider' ),
				array(
					'label' 				=> true,
					'placeholder' 			=> 'minimaal 3000',
					'defaultValue'			=> $this->getField( 'sliderSpeed' ),
				)
			),
			'wrapAround'	=> Field::checkbox(
				'wrapAround',
				__( 'Loop deze slider', 'chefslider' ),
				array(
					'defaultValue'			=> $this->getField( 'wrapAround', 'false' )
				)
			),
			'navButtons'	=> Field::checkbox(
				'navButtons',
				__( 'Zet navigatie pijlen aan', 'chefslider' ),
				array(
					'defaultValue'			=> $this->getField( 'navButtons', 'true' )
				)
			),
			'paginationThumb'	=> Field::select(
				'paginationThumb',
				__( 'Pagination', 'chefslider' ),
				$view,
				array(
					'defaultValue' => $this->getField( 'paginationThumb', 'dots' )
				)
			),
			'cellalign'	=> Field::select(
				'cellAlign',
				__( 'Uitlijning', 'chefslider' ),
				$alignment,
				array(
					'defaultValue' => $this->getField( 'cellAlign', 'left' )
				)
			)

		);

		$fields = apply_filters( 'chef_sections_collection_side_fields', $fields );

		return $fields;


	}
	
	/**
	 * Build the class-string for this column
	 * 
	 * @return string
	 */
	public function sliderClass(){
		$slides = $this->getField( 'numberSlides' );
		$arrows = $this->getField( 'navButtons' );

		$class = "main-gallery fullwidth gallery-slider ".$slides;

		if ( $arrows !== 'false' )
			$class .= ' nav-arrows-true';

		return $class;
	}

}
